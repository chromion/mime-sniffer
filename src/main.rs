use hex;
use std::env;
use std::fs;
use std::io;
use std::io::Read;

const LENGTH: usize = 24;

// file signatures: https://en.wikipedia.org/wiki/List_of_file_signatures
const FILE_SIGNATURES: &'static [(&str, &str)] = &[
    ("474946383961", "gif"),
    ("474946383761", "gif"),
    ("89504E470D0A1A0A", "png"),
    ("FFD8FF", "jpg"),
    ("57454250", "webp"),
    ("49492A00", "tiff"),
    ("4D4D002A", "tiff"),
    ("424D", "bmp"),
    ("000000146674797069736F6D", "mp4"),
    ("000000186674797033677035", "mp4"),
    ("000000146674797071742020", "mov"),
    ("1A45DFA3", "webm"),
    ("FFFB", "mp3"),
    ("494433", "mp3"),
    ("4F676753", "ogg"),
    ("664C6143", "flac"),
    ("52494646", "wav"),
    ("25504446", "pdf"),
];

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];

    println!("file: {}", filename);

    let mut file = fs::File::open(filename)?;

    let mut buffer = [0; LENGTH];
    file.read(&mut buffer)?;

    let hex_string = hex::encode(buffer);

    println!("hex(first {} bytes): {}", LENGTH, hex_string);

    for &(hex, filetype) in FILE_SIGNATURES.iter() {
        if hex_string.to_lowercase().contains(&hex.to_lowercase()) {
            println!("filetype: {}", filetype);
        }
    }

    Ok(())
}
