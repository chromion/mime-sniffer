# mime sniffer

This is a example project to demonstrate mime sniffing. Basically it reads the first bytes of a
file converts it to a hex string and checks it against key value store of [file signatures](https://en.wikipedia.org/wiki/List_of_file_signatures).


Mime sniffing was used by some internet browser but it exposed a security vulnerability where the
file is interpreting as an executable. It can be explicity disabled by provide the header:

```
X-Content-Type-Options: nosniff
```
